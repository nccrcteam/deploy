
set :rails_env, "staging"

set :user, "pgcasa"
set :repository, "git@bitbucket.org:gcarney/pgcasa.git"

set :deploy_to,   "/home/#{user}/#{application}"
set :copy_remote_dir, "/home/#{user}/tmp"
set :user_config_dir, "/home/#{user}/config"

role :app, "50.116.52.12"
role :web, "50.116.52.12"
role :db,  "50.116.52.12", :primary => true

# custom deploy tasks
namespace :deploy do

  desc "Set file and directory permissions."
  task :compile_assets, :roles => [:web, :app] do
  end

end

