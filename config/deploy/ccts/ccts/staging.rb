
set :rails_env, "staging"

set :user, "ccts"
set :repository, "git@bitbucket.org:gcarney/ccts_website.git"

set :deploy_to,   "/home/#{user}/#{application}"
set :copy_remote_dir, "/home/#{user}/tmp"
set :user_config_dir, "/home/#{user}/config"

set :asset_directories, %w(uploaded_images uploaded_files slides)

role :app, "ccts.staging.cctsbaltimore.org"
role :web, "ccts.staging.cctsbaltimore.org"
role :db,  "ccts.staging.cctsbaltimore.org", :primary => true

# custom deploy tasks
namespace :deploy do

  desc "Set file and directory permissions."
  task :compile_assets, :roles => [:web, :app] do
  end

  task :make_assets_dir, :roles => [:app] do
    run <<-CMD
      mkdir -p #{shared_path}/assets &&
      mkdir -p #{shared_path}/assets/uploaded_images &&
      mkdir -p #{shared_path}/assets/uploaded_files &&
      mkdir -p #{shared_path}/assets/slides
    CMD
  end

  task :finalize_update, :except => { :no_release => true }, :roles => [:web, :app] do
    run "chmod -R g+w #{latest_release}" if fetch(:group_writable, true)

    run <<-CMD
      mkdir -p #{latest_release}/tmp &&
      ln -s #{shared_path}/log #{latest_release}/log &&
      ln -s #{shared_path}/pids #{latest_release}/tmp/pids  &&
      ln -s #{shared_path}/ruby #{latest_release}/vendor/ruby
    CMD

    asset_directories.each do |dir|
      run "ln -s #{shared_path}/assets/#{dir} #{latest_release}/public/#{dir}"
    end
  end

end

