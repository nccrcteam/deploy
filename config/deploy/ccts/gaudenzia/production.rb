
set :rails_env, "production"

set :user, "gaudenzia"
set :repository, "git@bitbucket.org:gcarney/gaudenzia.git"
set :rvm_ruby_string, 'ruby-1.8.7-p358'

set :deploy_to,       "/home/#{user}/#{application}"
set :copy_remote_dir, "/home/#{user}/tmp"
set :user_config_dir, "/home/#{user}/config"

role :app, "web001a.yostlii.com"
role :web, "web001a.yostlii.com"
role :db,  "web001a.yostlii.com", :primary => true

namespace :deploy do
  desc "Compile Rails assets."                                                       
  task :compile_assets, :roles => [:web, :app] do                                    
  end      

  desc "Run bundle command."                                                         
  task :run_bundler, :except => { :no_release => true }, :roles => [:app] do         
  end                                           

  task :install_gems, :roles => [:app] do
    run "gem install unicorn --version 4.3.1 --no-ri --no-rdoc"
    run "gem install rmagick --version 1.15.14 --no-ri --no-rdoc"
    run "gem install mysql --version 2.8.1 --no-ri --no-rdoc"
  end
end
