
set :rails_env, "production"

set :user, "namimd"
set :repository, "git@bitbucket.org:gcarney/nami.git"

set :deploy_to,   "/home/#{user}/#{application}"
set :copy_remote_dir, "/home/#{user}/tmp"
set :user_config_dir, "/home/#{user}/config"

host = 'li910-172.members.linode.com'
role :app, host
role :web, host
role :db,  host, :primary => true

# custom deploy tasks
namespace :deploy do

  desc "Set file and directory permissions."
  task :compile_assets, :roles => [:web, :app] do
  end

end

